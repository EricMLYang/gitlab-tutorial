# GitlabTutorial
This project is all about gitlab self-learning record for stepping into MLOps world.
* Goal:
    * knowledge collection
    * Share package, tutorial & API for avoiding duplicating work
    * The foundation of MLOps

## Standard tool
* Python with anaocnda
* Git
* Jupyter lab
* Vscode on local
* .md with Markdown for doc.
* bash
* ...

## Separate your function from jupyter
* Create new directory for putting your gneneral function
* Let your package can be impore by your all project:
    * In each file head: sys.path.append(/home/your_directory/Packages)
    * In site-package: create a ooxx.pth (ex: package.pth) file and add path into it
* If a package is worth to share
    1. Create a Gitlab repo
    2. Clone to your Package
    3. Put your code into local
    4. push
    5. Announce on Data Team
* Follow clean code, clean architecture & best priatice 


## Project type & name on Gitlab
* Specific Project: Named by project name (Ex: MI)
* Persional: Named by NT account(Ex: ericmlyang)
* Tutorial: given some defautl project, please push your tutirial into related Project 
        * Tutorial_Probability_Statistic
        * Tutorial_OthersMath
        * Tutorial_Data_PreProcess
        * Tutorial_Business_Econometrics_TimeSeries
        * Tutorial_Machine_Learning
        * Tutorial_Deep_Learning
        * Tutorial_CleanCode_Python
        * Tutorial_MLOps
        * Tutorial_Linux_VM
        * Tutorial_DataBase
        * Tutorial_Others
* Package: Name your project by the main function (Ex: PyDB)


## Git
* Git is a great method to connect diffenet work, environment ...etc

### Git Branch Merging Best Practices
1. After you've selected a feature to work on, create a branch in your local repo to build it in.
    ```$ git checkout -b short_description_of_feature```
2. Implement the requested feature, make sure all tests are passing, and commit all changes in the new branch.
3. Checkout the master branch locally.
    ```$ git checkout master```
4. Pull down the master branch from GitLab to get the most up to date changes from others. If you practice git workflow as described here you should never have a merge conflict at this step.
    ```$ git pull origin master```
5. Make sure all tests are passing on master and then checkout your new branch.
    ```$ git checkout short_description_of_feature```
6. From your new branch, merge in your local master branch.
    ```$ git merge master```
7. Resolve any merge conflicts, make sure all tests are passing on the new branch, and then commit all changes from the merge.
    ```$ git add .```
    ```$ git commit -m "Merge in master."```
8. Local master merge branch
    ```$ git checkout master```
    ```$ git merge short_description_of_feature```
8. Push the local master to the remote repo.
    ```$ git push```


### Git commit message:
* The seven rules of a great Git commit messgae
    1. Separate subject from body with a blank line
    2. Limit the subject line to 50 characters
    3. Capitalize the subject line
    4. Do not end the subject line with a period
    5. Use the imperative mood in the subject line
    6. Wrap the body at 72 characters
    7. Use the body to explain what and why vs. how

* type 只允許使用以下類別：
    * feat: 新增/修改功能 (feature)。
    * fix: 修補 bug (bug fix)。
    * docs: 文件 (documentation)。
    * style: 格式 (不影響程式碼運行的變動 white-space, formatting, missing semi colons, etc)。
    * refactor: 重構 (既不是新增功能，也不是修補 bug 的程式碼變動)。
    * perf: 改善效能 (A code change that improves performance)。
    * test: 增加測試 (when adding missing tests)。
    * revert: 撤銷回覆先前的 commit 例如：revert: type(scope): subject (回覆版本：xxxx)。type 只允許使用以下類別：

* Git commit message template:
```
Header: <type>(<scope>): <subject>
 - type: 代表 commit 的類別：feat, fix, docs, style, refactor, test, chore，必要欄位。
 - scope 代表 commit 影響的範圍，例如資料庫、控制層、模板層等等，視專案不同而不同，為可選欄位。
 - subject 代表此 commit 的簡短描述，不要超過 50 個字元，結尾不要加句號，為必要欄位。

Body: 72-character wrapped. This should answer:
 * Body 部份是對本次 Commit 的詳細描述，可以分成多行，每一行不要超過 72 個字元。
 * 說明程式碼變動的項目與原因，還有與先前行為的對比。

Footer: 
 - 填寫任務編號（如果有的話）.
 - BREAKING CHANGE（可忽略），記錄不兼容的變動，
   以 BREAKING CHANGE: 開頭，後面是對變動的描述、以及變動原因和遷移方法。
```

##Giglab default content:
-----------------------------------------------------------------------------
## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/EricMLYang/gitlab-tutorial.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:b96c8c306adfe7497ffc558bdd185db3?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

